from .generator import Generator


class MersenneGenerator(Generator):
    def __init__(self, seed):
        super().__init__()
        self.config_values = self.config['mersenne']
        self.n = self.config_values['n']
        self.MT = [0 for _ in range(self.n)]
        self.index = self.n + 1
        self.lower_mask = (1 << self.config_values['r']) - 1
        self.upper_mask = self.lower_mask ^ 0xFFFFFFFF

        self.index = self.n
        self.MT[0] = seed
        for i in range(1, self.n - 1):
            self.MT[i] = (self.config_values['f'] * (self.MT[i - 1] ^
                                                     (self.MT[i - 1] >> (self.config_values['w'] - 2))) + i) & 0xFFFFFFFF

    def next(self):
        if self.index >= self.n:
            self._twist()

        y = self.MT[self.index]

        y = y ^ ((y >> self.config_values['u']) & self.config_values['d'])
        y = y ^ ((y << self.config_values['s']) & self.config_values['b'])
        y = y ^ ((y << self.config_values['t']) & self.config_values['c'])
        y = y ^ (y >> self.config_values['l'])

        self.index += 1

        return y & 0xFFFFFFFF 

    def _twist(self):
        for i in range(0, self.n - 1):
            x = (self.MT[i] & self.upper_mask) + \
                (self.MT[(i+1) % self.n] & self.lower_mask)
            x_a = x >> 1

            if x % 2 != 0:
                x_a ^= self.config_values['a']

            self.MT[i] = self.MT[(
                i + self.config_values['m']) % self.n] ^ x_a

        self.index = 0
