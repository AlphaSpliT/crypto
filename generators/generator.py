import struct
from math import ceil


class Generator:
    def __init__(self):
        self.config = {
            'p': 15_485_863,
            'q': 32_452_843,
            'w': 5,
            'mersenne': {
                'w': 32,
                'n': 624,
                'm': 397,
                'r': 31,
                'a': 0x9908B0DF,
                'u': 11,
                'd': 0xFFFFFFFF,
                's': 7,
                'b': 0x9D2C5680,
                't': 15,
                'c': 0xEFC60000,
                'l': 18,
                'f': 1812433253
            },
            'lagged': {
                'j': 7,
                'k': 10,
                'm': 2 ** 32
            },
            'lcg': {
                'm': 2 ** 32,
                'a': 1664525,
                'c': 1013904223
            }
        }

    def next(self):
        raise Exception('Not implemented!')

    def gen_bytes(self, number_of_bytes):
        return b''.join([struct.pack('B', self.gen_byte()) for _ in range(number_of_bytes)])
    
    def isprime(self, n):
        if (n <= 1):
            return False
        if (n <= 3):
            return True

        if (n % 2 == 0 or n % 3 == 0):
            return False

        i = 5
        while(i * i <= n):
            if (n % i == 0 or n % (i + 2) == 0):
                return False
            i = i + 6

        return True

    def gen_byte(self):
        return self.next() & 0xFF