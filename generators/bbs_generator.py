# Blum Blum Shub
from .generator import Generator
import struct

class BBSGenerator(Generator):
    def __init__(self, seed):
        super().__init__()

        self.p = self.config['p']
        assert self.isprime(self.p)

        self.q = self.config['q']
        assert self.isprime(self.q)

        self.w = self.config['w']
        assert self.w >= 1

        self.M = self.p * self.q
        self.current_value = seed

    def next(self):
        result_value = 0
        for _ in range(self.w):
            self.current_value = pow(self.current_value, 2, self.M)
            result_value <<= 1
            result_value += self.current_value
        
        return result_value & 0xffffffff