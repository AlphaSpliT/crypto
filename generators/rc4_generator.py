import struct

from .generator import Generator


class RC4Generator(Generator):
    def __init__(self, seed, N=256):
        super().__init__()
        self.seed = str(seed).encode('utf-8')
        self.i = 0
        self.j = 0
        self.N = 256
        self.s = [i for i in range(0, N)]

        j = 0
        for i in range(0, N):
            j = (j + self.s[i] + self.seed[i % len(seed)]) % N
            self.s[i], self.s[j] = self.s[j], self.s[i]

    def random_native(self):
        self.i = (self.i + 1) % self.N
        self.j = (self.j + self.s[self.i]) % self.N

        self.s[self.i], self.s[self.j] = self.s[self.j], self.s[self.i]

        return self.s[(self.s[self.i] + self.s[self.j]) % self.N]

    def next(self):
        a = self.random_native()
        b = self.random_native()
        c = self.random_native()
        d = self.random_native()

        return ((a * 256 + b) * 256 + c) * 256 + d

    def gen_bytes(self, num_of_bytes):
        return b''.join([struct.pack('B', self.random_native()) for _ in range(num_of_bytes)])
