from .generator import Generator


class XORShiftGenerator(Generator):
    def __init__(self, seed):
        super().__init__()
        self.seed = seed

    def next(self):
        self.seed ^= self.seed << 13
        self.seed ^= self.seed >> 17
        self.seed ^= self.seed << 5
        self.seed &= 0xFFFFFFFF
        return self.seed
