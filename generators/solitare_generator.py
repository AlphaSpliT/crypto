from .generator import Generator


class SolitareGenerator(Generator):
    def __init__(self, seed):
        if type(seed) != str:
            raise Exception('Seed must be of type STR')
        
        if len(seed) < 64:
            raise Exception('It is recommended to provide a seed of at least 64 characters!')

        self.seed = seed.upper()
        self.cards = [str(i) for i in range(1, 53)]
        self.cards.append('A')
        self.cards.append('B')
        self.length = len(self.cards) - 1

        for c in self.seed:
            to_cut_index = ord(c) - ord('A') + 1
            self.find_A_move_down()
            self.find_B_move_down()
            self.triple_cut()
            self.count_cut_by_number(to_cut_index)


    def next(self):
        self.do_one_shuffle()
        return_value = self.cards[self.convert_index_to_number(0)]

        while return_value == 'A' or return_value == 'B':
            self.do_one_shuffle()
            return_value = self.cards[self.convert_index_to_number(0)]

        return int(return_value)

    def do_one_shuffle(self):
        self.find_A_move_down()
        self.find_B_move_down()
        self.triple_cut()
        self.count_cut()
    

    def find_A_move_down(self):
        index = self.cards.index('A')

        if self.is_last(index):
            self.cards.remove('A')
            self.cards.insert(1, 'A')
        else:
            self.cards[index], self.cards[index +
                                          1] = self.cards[index + 1], self.cards[index]

    def find_B_move_down(self):
        index = self.cards.index('B')

        if self.is_last(index):
            self.cards.remove('B')
            self.cards.insert(2, 'B')
        elif self.is_second_before_last(index):
            self.cards.remove('B')
            self.cards.insert(1, 'B')
        else:
            self.cards[index], self.cards[index +
                                          1] = self.cards[index + 1], self.cards[index]
            self.cards[index + 1], self.cards[index +
                                              2] = self.cards[index + 2], self.cards[index + 1]

    def triple_cut(self):
        indexA = self.cards.index('A')
        indexB = self.cards.index('B')

        if indexA > indexB:
            indexA, indexB = indexB, indexA

        above_first = self.cards[:indexA]
        below_second = self.cards[indexB + 1:]
        between = self.cards[indexA:indexB + 1]

        self.cards = [*below_second, *between, *above_first]

    def count_cut(self):
        count = self.convert_last_to_nubmer()

        self.count_cut_by_number(count)

    def count_cut_by_number(self, number):
        to_cut = self.cards[:number]
        remaining = self.cards[number:-1]
        last = self.cards[-1]

        self.cards = [*remaining, *to_cut, last]

    def is_last(self, index):
        return self.length == index

    def is_second_before_last(self, index):
        return self.length - 1 == index

    def convert_last_to_nubmer(self):
        return self.convert_index_to_number(-1)

    def convert_index_to_number(self, index):
        value_on_index = self.cards[index]
        return self.convert_value_to_number(value_on_index)

    def convert_value_to_number(self, value):
         return 53 if value == 'A' or value == 'B' else int(value)
