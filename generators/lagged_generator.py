from .generator import Generator


class LaggedGenerator(Generator):
    def __init__(self, seed):
        super().__init__()
        assert len(seed) == self.config['lagged']['k']
        self.j = self.config['lagged']['j']
        self.k = self.config['lagged']['k']
        self.seed_array = [int(c) for c in seed]

    def next(self):
        out = (self.seed_array[self.j-1] +
               self.seed_array[self.k-1]) % self.config['lagged']['m']

        for i in range(len(self.seed_array) - 1):
            self.seed_array[i] = self.seed_array[i + 1]

        self.seed_array[len(self.seed_array) - 1] = out

        return out & 0xFFFFFFFF
