from .generator import Generator
from .lcg_generator import LCGGenerator
from math import floor, sqrt

# 2D LCGD


class FeriGenerator(Generator):
    def __init__(self, seed):
        if type(seed) != int:
            raise "Seed can only be of type INT"

        self.previous_x = 0
        self.previous_y = 0
        self.x_ordinate_generator = LCGGenerator(seed)
        self.y_ordinate_generator = LCGGenerator(abs(0xFFFFFFFF - seed))

    def next(self):
        new_x = self.x_ordinate_generator.next()
        new_y = self.y_ordinate_generator.next()
        return_val = int(FeriGenerator.dist(
            new_x, new_y, self.previous_x, self.previous_y))
        self.previous_x = new_x
        self.previous_y = new_y

        return return_val

    @staticmethod
    def dist(x1, y1, x2, y2):
        return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
