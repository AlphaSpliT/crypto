import numbers


class Vec2D:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def add(self, rhs, modulo=0):
        if isinstance(rhs, numbers.Number):
            self.x += rhs
            self.y += rhs
        elif type(rhs) == Vec2D:
            self.x += rhs.x
            self.y += rhs.y

        if modulo != 0:
            self.x %= modulo
            self.y %= modulo

        return self

    def sub(self, rhs, modulo=0):
        if isinstance(rhs, numbers.Number):
            self.x -= rhs
            self.y -= rhs
        elif type(rhs) == Vec2D:
            self.x -= rhs.x
            self.y -= rhs.y

        if modulo != 0:
            self.x %= modulo
            self.y %= modulo

        return self

    def mul(self, rhs, modulo=0):
        if isinstance(rhs, numbers.Number):
            self.x *= rhs
            self.y *= rhs
        elif type(rhs) == Vec2D:
            self.x *= rhs.x
            self.y *= rhs.y

        if modulo != 0:
            self.x %= modulo
            self.y %= modulo

        return self

    def div(self, rhs):
        if isinstance(rhs, numbers.Number):
            self.x /= rhs
            self.y /= rhs

        return self

    def __str__(self):
        return f'<x={self.x} y={self.y}/>'