from .generator import Generator
from os import urandom
from struct import unpack

class URandomGenerator(Generator):
    def next(self):
        return unpack('I', urandom(4))[0]