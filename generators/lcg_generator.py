from .generator import Generator

class LCGGenerator(Generator):
    def __init__(self, seed):
        super().__init__()
        self.seed = seed
        self.config_params = self.config['lcg']

    def next(self):
        self.seed = (self.config_params['a'] * self.seed + self.config_params['c']) % self.config_params['m']
        return self.seed