from generators.bbs_generator import BBSGenerator
from generators.mersenne_generator import MersenneGenerator
from generators.lagged_generator import LaggedGenerator
from generators.lcg_generator import LCGGenerator
from generators.xorshift_generator import XORShiftGenerator
from generators.urandom_generator import URandomGenerator
from generators.rc4_generator import RC4Generator
from generators.feri_generator import FeriGenerator
from generators.solitare_generator import SolitareGenerator

import sys

generators = {
    'mersenne': MersenneGenerator(4043855564),
    'bbs': BBSGenerator(15),
    'lagged': LaggedGenerator('2045828613'),
    'lcg': LCGGenerator(3),
    'xorshift': XORShiftGenerator(15),
    'urandom': URandomGenerator(),
    'rc4': RC4Generator("helloszianagyonkemeny"),
    'feri': FeriGenerator(2500100),
    'solitare': SolitareGenerator('EKHFFRNAXPMVHUTINNVDLZVOIKJTCRZZLOMOHJXHZAMSLRSMPPKWLCTKUBJNGKWD')
}

# with open('my_generated.input', 'w') as f:
#     f.write("""type: d
# count: 10000000
# numbit: 32
# """)
#     to_write = ''
#     for i in range(10000000):
#         if i % 10000 == 0:
#             print(i / 10000000 * 100)
        
#         if i % 10000000 == 0:
#             f.write(to_write)
#             to_write = ''
        
#         die_nexte = generators["mersenne"].next()
#         to_write += ' ' * (10 - len(str(die_nexte)))
#         to_write += f'{die_nexte}\n'
    
#     f.write(to_write)

# print(len(generators['mersenne'].gen_bytes(134217728)))

with open('binary_output.input', 'wb') as f:
    remaining = 134217728
    while remaining > 8192:
        remaining -= 8192
        f.write(generators['lcg'].gen_bytes(8192))
        sys.stdout.write('\r')
        sys.stdout.write(f'{(remaining / 134217728) * 100}%')
        sys.stdout.flush()
    f.write(generators['lcg'].gen_bytes(remaining))

    

