# Crypto Pseudo-Random generators

Main.py is mainly used for generation and saving into files. The actual generatos can be found in the `generators` folder. Each written generator implements the base class Generator which defines some virtual methods which should be overwritten. List of generators:

- Blum blum Shub
- Lagged Fibonacci
- Linear congruential generator (LCG)
- Mersenne Twister (MT19937)
- XorShift
- urandom (python arch implementation /dev/urandom)
- Rivest Cipher 4 (RC4)
- Solitare generator
- Feri generator

In the excel document provided below, you can review the test results I've gotten, a small description of the test cases I used and also the algorithms.
[Excel Spreadsheet](https://scsubbclujro-my.sharepoint.com/:x:/g/personal/afim1689_scs_ubbcluj_ro/ERKMfVq6WgNDrwin64iuTdgBg44Fh5zLLBGGOXnB46RLuA?e=5YHpxh)
